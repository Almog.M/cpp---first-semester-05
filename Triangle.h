#pragma once
#include "Polygon.h"
#include "Point.h"


class Triangle : public Polygon
{
public:
	Triangle(const Point& a, const Point& b, const Point& c, const string& type, const string& name);
	virtual ~Triangle();
	virtual double getArea() const; //By Heron's formula.
	virtual double getPerimeter() const;
	virtual void draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board); //Draws a shape in the painter.
	virtual void move(const Point& other); // add the Point to all the points of shape
	virtual void clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board); //Delete the shape from the painter.
	void printDetails() const;
private:
	Point _a;
	Point _b;
	Point _c;
	double _base;
	double _firstRib;
	double _secondRib;

	// override functions if need (virtual + pure virtual)
};