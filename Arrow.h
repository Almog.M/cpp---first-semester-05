#pragma once
#include "Polygon.h"
#include <Math.h>

class Arrow : public Shape
{
public:
	Arrow(const Point& a, const Point& b, const string& type, const string& name);
	~Arrow();

	virtual double getArea() const;
	virtual double getPerimeter() const;
	virtual void draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board); //Draws a shape in the painter.
	virtual void move(const Point& other); // add the Point to all the points of shape
	void printDetails() const; //Prints the fields of the shape.
	virtual void clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board); //Delete the shape from the painter.

	// override functions if need (virtual + pure virtual)

private:
	Point _p1;
	Point _p2;
};