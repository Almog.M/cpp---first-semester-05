#include "Polygon.h"

Polygon::Polygon(const string & type, const string & name) : Shape(name, type)
{

}

Polygon::~Polygon()
{
	this->_points.clear();
}

void Polygon::printDetails() const
{
	double xPoint = 0;
	double yPoint = 0;
	Shape::printDetails();
	for (unsigned int i = 0; i < this->_points.size(); i++)
	{
		xPoint = this->_points[i].getX();
		yPoint = this->_points[i].getY();
		std::cout << "The " << i + 1 << " point coordinations: ";
		std::cout << "X = " << xPoint << " Y = " << yPoint << std::endl;
	}
}
