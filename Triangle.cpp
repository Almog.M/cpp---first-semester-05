#include "Triangle.h"
#include <Math.h>


void Triangle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char GREEN[] = { 0, 255, 0 };
	board.draw_triangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(),
		_points[2].getX(), _points[2].getY(), GREEN, 100.0f).display(disp);
}

void Triangle::move(const Point & other)
{
	_a += other;
	_b += other;
	_c += other;
}

void Triangle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };
	board.draw_triangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(),
		_points[2].getX(), _points[2].getY(), BLACK, 100.0f).display(disp);
}
void Triangle::printDetails() const
{
	Polygon::printDetails();
	cout << "\nThe area of the triangle: " << this->getArea() << std::endl;
	cout << "\nThe perimeter of the triangle: " << this->getPerimeter() << std::endl;

}
Triangle::Triangle(const Point & a, const Point & b, const Point & c, const string & type, const string & name) : Polygon(type, name), _a(a), _b(b), _c(c)
{
	bool isNotTriamgle = (_a.getX() == _b.getX()) && (_a.getX() == _c.getX()) || (_a.getY() == _b.getY()) && (_a.getY() == _c.getY()); //If the coordinations are valid.
	if (isNotTriamgle)
	{
		throw "The coordinations are not creating a triangle...";
	}
	_points[0] = _a;
	_points[1] = _b;
	_points[2] = _c;


	_base = (double)abs(_a.distance(_b));
	_firstRib = (double)abs(_b.distance(_c));
	_secondRib = (double)abs(_a.distance(_c));


}

Triangle::~Triangle()
{
}

double Triangle::getArea() const
{
	double p = (_base + _firstRib + _secondRib) / 2;
	double area = sqrt(p * (p - _base) * (p - _firstRib) * (p - _secondRib));
	return area;
}

double Triangle::getPerimeter() const
{
	return _base + _firstRib + _secondRib;
}
