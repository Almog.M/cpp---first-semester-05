#include "Point.h"
#include <Math.h>

Point::Point(double x, double y)
{
	_x = x;
	_y = y;
}

Point::Point(const Point & other)
{
	_x = other._x;
	_y = other._y;
}

Point::~Point()
{
}

Point Point::operator+(const Point & other) const
{
	Point result(other._x, other._y);
	result._x += this->_x;
	result._y += this->_y;
	return result;
}

Point Point::operator-(const Point & other) const
{
	Point newOne(this->_x, this->_y);
	newOne._x -= other._x;
	newOne._y -= other._y;
	return newOne;
}

Point & Point::operator+=(const Point & other)
{
	this->_x += other._x;
	this->_y += other._y;
	return *this;
}

Point & Point::operator=(const Point & other)
{
	this->_x = other._x;
	this->_y = other._y;
	return *this;
}

double Point::getX() const
{
	return this->_x;
}

double Point::getY() const
{
	return this->_y;
}

void Point::setX(double x)
{
	this->_x = x;
}

void Point::setY(double y)
{
	this->_y = y;
}

double Point::distance(const Point & other) const
{
	double xDelta = other._x - this->_x;
	double yDelta = other._y - this->_y;
	double distance = sqrt(pow(xDelta, 2) + pow(yDelta, 2));

	return distance;
}
