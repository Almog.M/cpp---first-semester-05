#include "Rectangle.h"




myShapes::Rectangle::Rectangle(const Point & a, double length, double width, const string & type, const string & name) : Polygon(type, name)
{
	if (length <= 0 || width <= 0)
	{
		throw "One or more parameters are not valid";
	}
	_length = length;
	_width = width;
	_points[0] = a; //A
	_points[1] = a - Point(0, _length); //B
	_points[2] = _points[1] - Point(width, 0); //C
	_points[3] = _points[2] + Point(0 ,_length); //D
}

myShapes::Rectangle::~Rectangle()
{
}

double myShapes::Rectangle::getArea() const
{
	return _length * _width;
}

double myShapes::Rectangle::getPerimeter() const
{
	return 2 * _length + 2 * _width;
}

void myShapes::Rectangle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char WHITE[] = { 255, 255, 255 };
	board.draw_rectangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(), WHITE, 100.0f).display(disp);
}

void myShapes::Rectangle::move(const Point & other)
{
	_points[0] += other;
	_points[1] += other;
	_points[2] += other;
	_points[3] += other;
}

void myShapes::Rectangle::printDetails() const
{
	Polygon::printDetails();
	cout << "\nThe area of the rectangle: " << this->getArea() << std::endl;
	cout << "\nThe perimeter of the rectangle: " << this->getPerimeter() << std::endl;
}

void myShapes::Rectangle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0};
	board.draw_rectangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(), BLACK, 100.0f).display(disp);
}


