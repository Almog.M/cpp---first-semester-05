#include "Shape.h"
#include <iostream>
#include <string>

Shape::Shape(const std::string & name, const std::string & type)
{
	this->_name = name;
	this->_type = type;
}

Shape::~Shape()
{
	_graphicShape.clear();
}

void Shape::printDetails() const
{
	std::cout << "The name of the shape is: " << this->_name << std::endl;
	std::cout << "The type of the shape is: " << this->_type << std::endl;
}

std::string Shape::getType() const
{
	return _type;
}

std::string Shape::getName() const
{
	return _name;
}
