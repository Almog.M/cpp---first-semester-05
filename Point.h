#pragma once
#include <vector>
//#include "CImg.h"

class Point
{
public:
	Point(double x, double y);
	Point(const Point& other);
	virtual ~Point();
	
	Point operator+(const Point& other) const; //Adding the coordinations. x1 (of this.) + x2 (of the other point) = new x (the x of the return value). and the same for y values.
	Point operator-(const Point& other) const; //Subs the coordinations. x1 (of this.) - x2 (of the other point) = new x (the x of the return value). and the same for y values.
	
	Point& operator+=(const Point& other); //The same the above, but adding the other to the this object.
	Point& operator=(const Point& other); //Returns ref to - this.

	double getX() const; //About x coordination.
	double getY() const; //About y coordination.

	void setX(double x);
	void setY(double y);


	double distance(const Point& other) const;
private:
	double _x;
	double _y;
	
};