#pragma once
#include <iostream>
#include <string>
#include "Point.h"
#include "CImg.h"

using std::string;
using std::vector;
using std::cin;
using std::cout;

class Shape 
{
public:
	Shape(const string& name, const string& type);
	~Shape();
	virtual double getArea() const = 0;
	virtual double getPerimeter() const = 0;
	virtual void draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board) = 0; //Draws a shape in the painter.
	virtual void move(const Point& other) = 0; // add the Point to all the points of shape
	void printDetails() const; //Prints the fields of the shape.
	virtual void clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board) = 0; //Delete the shape from the painter.

	std::string getType() const;
	std::string getName() const;
private:
	std::string _name;
	std::string _type;

protected:
	
	std::vector<cimg_library::CImg<unsigned char>> _graphicShape; //A vector for draw a shape.
};